#!/bin/bash
# 
# Checkout: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-lamp-amazon-linux-2.html
#

# Defined version php
PHP="php7.4"

sudo yum update -y

echo "Install PHP on Ubuntu"
sudo amazon-linux-extras install -y ${PHP}

echo "Install Apache web server, MYSQL Client, Git, and PHP software packages."
sudo yum install -y httpd git mysql
sudo yum install -y php-opcache php-mbstring php-zip php-xml php-xmlrpc
# php-fpm -R

echo "Start the Apache web server."
sudo systemctl start httpd && sudo systemctl enable httpd

echo "You can verify that httpd is on by running the following command:"
sudo systemctl is-enabled httpd

echo "Add your user (in this case, ec2-user) to the apache group."
sudo usermod -a -G apache ec2-user
sudo chown -R ec2-user:apache /var/www

# Composer
echo "Downloading and Installing Composer"
cd /tmp/ && sudo curl -sS https://getcomposer.org/installer -o composer-setup.php
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
composer --version

# Node
echo "Downloading and Installing Node, NPM, YARN"
curl -sL https://rpm.nodesource.com/setup_14.x | sudo bash -
# sudo yum install gcc-c++ make
sudo yum install -y nodejs
curl -sL https://dl.yarnpkg.com/rpm/yarn.repo | sudo tee /etc/yum.repos.d/yarn.repo
sudo yum install yarn -y

echo "Need start PHP-FPM"
php-fpm -R

# Autoclean cache
sudo yum clean all