#!/bin/bash
# Install it as a yum package instead of throgh pip (will save you from lots of hassle)

# first get epel
sudo amazon-linux-extras install epel

# then install supervisor
sudo yum install supervisor -y

sudo service supervisord start