#!/bin/bash
# Defined version php
PHP="php7.4"

echo "Install PHP on Ubuntu"
sudo apt install -y ${PHP} ${PHP}-dev ${PHP}-common ${PHP}-cli ${PHP}-fpm ${PHP}-json ${PHP}-opcache ${PHP}-mysql ${PHP}-mbstring ${PHP}-zip ${PHP}-xml ${PHP}-xmlrpc

# Composer
echo "Downloading and Installing Composer"
cd /tmp/ && curl -sS https://getcomposer.org/installer -o composer-setup.php
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
composer --version

echo "Downloading and Installing PHP CS/PHP MD"
composer global require "squizlabs/php_codesniffer=*" "phpmd/phpmd"

# Autoclean cache
sudo apt autoremove -y
