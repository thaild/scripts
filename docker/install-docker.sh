#!/bin/bash
# Docker
echo "Install Docker on Ubuntu"
sudo apt-get install apt-transport-https ca-certificates curl -y &&\
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - &&\
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" &&\
sudo apt-get update -y && sudo apt-get install -y docker-ce &&\
sudo systemctl enable docker &&\
sudo usermod -aG docker ${USER}

# Docker-compose
echo "Install Docker-compose"
sudo curl -L https://github.com/docker/compose/releases/download/1.25.4/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Docker-cleanup
echo "Install Docker-cleanup"
cd /tmp
git clone https://gist.github.com/76b450a0c986e576e98b.git && cd 76b450a0c986e576e98b
sudo mv docker-cleanup /usr/local/bin/docker-cleanup
sudo chmod +x /usr/local/bin/docker-cleanup
