#!/bin/bash
# Add the following line to either /etc/sysctl.conf file or a new *.conf file (e.g. idea.conf) under /etc/sysctl.d/ directory:
# https://confluence.jetbrains.com/display/IDEADEV/Inotify+Watches+Limit

# sudo touch /etc/sysctl.d/idea.conf
# echo "fs.inotify.max_user_watches = 524288" >> /etc/sysctl.d/idea.conf

# OR
sudo sysctl fs.inotify.max_user_watches=524288
systemctl restart systemd-sysctl.service
