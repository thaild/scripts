## To create a new file template﻿


1. In the Settings/Preferences dialog Ctrl+Alt+S, select Editor | File and Code Templates.

2. On the Files tab, click the Create Template button and specify the name, file extension, and body of the template.

3. Apply the changes and close the dialog.


## PHP File Header.php

@ref: https://www.jetbrains.com/help/phpstorm/using-file-and-code-templates.html

```php
/**
* ${FILE_NAME}
* Created by PhpStorm
* User: ${USER}
* Date: ${DAY}/${MONTH}/${YEAR}
* Time: ${HOUR}:${MINUTE}
*/
```
