#!/bin/bash
# tested on ubuntu 18.04

# Defined version php
PHP="php7.2"

sudo apt install -y apache2 mysql-server mysql-client
#CREATE USER 'fooder'@'%' IDENTIFIED BY 'f00d3r@passwd';
#GRANT ALL PRIVILEGES ON *.* TO 'fooder'@'%';

echo "Install PHP on Ubuntu"
sudo apt install -y ${PHP} ${PHP}-dev ${PHP}-common ${PHP}-cli ${PHP}-fpm ${PHP}-json ${PHP}-opcache ${PHP}-mysql php-mbstring php-curl ${PHP}-zip ${PHP}-xml ${PHP}-xmlrpc

# Composer
echo "Downloading and Installing Composer"
cd /tmp/ && curl -sS https://getcomposer.org/installer -o composer-setup.php
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
composer --version

# Auto clean cache
sudo apt autoremove -y
