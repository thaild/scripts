#!/bin/bash
sudo -s
apt install fail2ban -y
cp -pf /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

#sudo systemctl status fail2ban
systemctl enable fail2ban
systemctl start fail2ban

sleep 3

# Check status
fail2ban-client status sshd
