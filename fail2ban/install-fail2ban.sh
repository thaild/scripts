#!/bin/bash
###############################################
# https://gitlab.com/thaild/scripts/-/raw/master/fail2ban
# To use:
# curl --output install-fail2ban.sh https://gitlab.com/thaild/scripts/-/raw/master/fail2ban/install-fail2ban.sh
# sudo chmod +x install-fail2ban.sh
# sudo ./install-fail2ban.sh
###############################################

PACKAGE="yum"
system_version=$(cat /etc/*-release | grep "VERSION_ID=" | cut -f2 -d'"' | xargs)
OS=$(hostnamectl | grep "Operating System" | cut -f2 -d":" | cut -f2 -d' '  | xargs)

if [[ "$system_version" != "8" ]]; then
    if [[ "$OS" != "Amazon" ]] && [[ "$OS" != "Rocky" ]] && [[ $OS != *"Linux"* ]]; then
        PACKAGE="apt"
    fi
fi

hostnamectl | grep "Operating System"
echo "Using $PACKAGE"

$PACKAGE install fail2ban -y
cp -pf /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

GIT_SHA="master"

SSHD_LOCAL="/etc/fail2ban/jail.d/sshd.local"
INTEGRATION_LOCAL="/etc/fail2ban/action.d/integration-notifications.local"

curl --output ${SSHD_LOCAL} https://gitlab.com/thaild/scripts/-/raw/${GIT_SHA}/fail2ban/jail.d/sshd.conf
curl --output ${INTEGRATION_LOCAL} https://gitlab.com/thaild/scripts/-/raw/${GIT_SHA}/fail2ban/action.d/integration-notifications.conf

## Check webhook inputs
echo "Webhook"
read -p "Input webhook: " webhook

echo "Channel"
read -p "Input channel: " channel

echo "Hostname"
read -p "Input hostname: " hostname

sed -i "s/channel=/channel=$channel/g" ${INTEGRATION_LOCAL}
sed -i "s/webhook_url=/webhook_url=$webhook/g" ${INTEGRATION_LOCAL}
sed -i "s/hostname=/hostname=$hostname/g" ${INTEGRATION_LOCAL}

echo "Configuration Fail2ban succeeded"

#sudo systemctl status fail2ban
systemctl enable fail2ban
systemctl start fail2ban
systemctl restart fail2ban

sleep 3

# Check status
fail2ban-client status sshd
