#!/bin/bash
sudo -s
yum install epel-release
yum install fail2ban -y
cp -pf /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

systemctl enable fail2ban
systemctl start fail2ban

sleep 3

fail2ban-client status sshd
